//
//  HBStarter.m
//  anotherTest
//
//  Created by nus on 6/23/15.
//  Copyright (c) 2015 NUS. All rights reserved.
//

#import "HBStarter.h"
#import "mainEntry.h"
#import "recorderMode.h"

@interface HBStarter ()
@property (weak, nonatomic) IBOutlet UIButton *nextStep;
@property (weak, nonatomic) IBOutlet UIButton *readyButton;
@property (weak, nonatomic) IBOutlet UILabel *HBmonitor;
@property (weak, nonatomic) IBOutlet UILabel *timeMonitor;
@property (weak, nonatomic) IBOutlet UILabel *nextStepReadyLabel;

@property int myAvrgeHB;
@property BOOL firstPressReady;
@property BOOL startingTimeStampBOOL;
@property int startingTimeStamp;
@end

@implementation HBStarter

int aHBcounter;
int aHBStore[2000];
int aavrgHB;

- (IBAction)readyToDetect:(UIButton *)sender        //The READY button
{
    if(self.firstPressReady)
    {
        //NSLog(@"%@",self.oldDiscoveredPeripheral.name);
        //[self.oldDiscoveredPeripheral discoverServices:nil];
        aHBcounter = 0;     //Reset the counter to 0 so that it will count from average.
        self.firstPressReady = NO;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    aHBcounter = 0;
    aavrgHB = 0;
    
    UIEdgeInsets insets = UIEdgeInsetsMake(18, 18, 18, 18);
    UIImage *nextStepImage = [UIImage imageNamed:@"blueButton@2x.png"];
    UIImage *stretchableNextStepImage = [nextStepImage resizableImageWithCapInsets:insets];
    [self.nextStep setBackgroundImage:stretchableNextStepImage forState:UIControlStateNormal];
    
    UIImage *readyButtonImage = [UIImage imageNamed:@"greenButton@2x.png"];
    UIImage *stretchableReadyButtonImage = [readyButtonImage resizableImageWithCapInsets:insets];
    [self.readyButton setBackgroundImage:stretchableReadyButtonImage forState:UIControlStateNormal];
    self.firstPressReady = YES;
    self.oldDiscoveredPeripheral.delegate = self;
    self.startingTimeStampBOOL = YES;
    [self.oldDiscoveredPeripheral discoverServices:nil];        //changed by Nic
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if(error)
    {
        NSLog(@"%@",[error description]);
        return;
    }
    for (CBService *service in peripheral.services)
    {
        //NSLog(@"Discovered Service: %@",[service description]);
        [peripheral discoverCharacteristics:nil forService:service];
    }
}
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if(error)
    {
        NSLog(@"%@",[error description]);
        return;
    }
    for(CBCharacteristic *characteristic in service.characteristics)
    {
        //[self tLog:[NSString stringWithFormat:@"Characteristic Found: %@",[characteristic description]]];
        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        
    }
}
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if(error)
    {
        //[self tLog:[error description]];
        return;
    }
    
    /////
    int byte1, byte2, byte3, byte4, byte5, byte6;
    [characteristic.value getBytes:&byte1 range:NSMakeRange(1, 1)];
    [characteristic.value getBytes:&byte2 range:NSMakeRange(3, 1)];
    [characteristic.value getBytes:&byte3 range:NSMakeRange(5, 1)];
    [characteristic.value getBytes:&byte4 range:NSMakeRange(6, 1)];
    [characteristic.value getBytes:&byte5 range:NSMakeRange(8, 1)];
    [characteristic.value getBytes:&byte6 range:NSMakeRange(10, 1)];
    uint32_t time = 0x00;
    uint32_t temp = 0x00;
    temp = (byte1&0xF0)>>4;
    temp = (temp & 0x0F);
    time = time | temp;
    temp = 0x00;
    temp = (byte2&0xF0)>>4;
    temp = (temp & 0x0F)<<4;
    time = time | temp;
    temp = 0x00;
    temp = (byte3&0xF0)>>4;
    temp = (temp & 0x0F)<<8;
    time = time | temp;
    temp = 0x00;
    temp = (byte4&0x0F);
    temp = (temp & 0x0F)<<12;
    time = time | temp;
    temp = 0x00;
    temp = (byte5&0x0F);
    temp = (temp & 0x0F)<<16;
    time = time | temp;
    temp = 0x00;
    temp = (byte6&0x0F);
    temp = (temp & 0x0F)<<20;
    time = time | temp;
    float time_float = time/1000;
    int LSByte;
    [characteristic.value getBytes:&LSByte range:NSMakeRange(18,1)];
    uint8_t heartrate = 0x00;
    heartrate = heartrate|LSByte;
    
    if(self.startingTimeStampBOOL)
    {
        self.startingTimeStamp = (int)time_float;
        self.startingTimeStampBOOL = NO;
    }
    
    self.HBmonitor.text = [NSString stringWithFormat:@"%d BPM", heartrate];
    
    int seconds = ((int)time_float-self.startingTimeStamp) % 60;
    int minutes = (((int)time_float-self.startingTimeStamp) / 60) % 60;
    int hours = ((int)time_float-self.startingTimeStamp) / 3600;
    [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    self.timeMonitor.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    
    if((int)time_float-self.startingTimeStamp < 0)
    {
        return;
    }
    //////////////////////////////////////
    //////////////////////////////////////
    //Getting Resting HB!!!!!!!!!!!!!!!!//
    //////////////////////////////////////
    //////////////////////////////////////
    if(aHBcounter<2000)
    {
        aHBStore[aHBcounter] = heartrate;
        aHBcounter++;
    }
    if(aHBcounter>=2000)        //Taking average of 2000 values of BPM
    {
        aHBcounter = 0;    //reset the counter
        if(!self.firstPressReady)       //only when the button is clicked then move on.
        {
            self.nextStepReadyLabel.text = @"Ready to Move to the Next Step...";
            //self.HBmonitor.text = [NSString stringWithFormat:@"%d BPM", aavrgHB];
            
            
            aavrgHB = aHBStore[1999];       //Taking the last value of the heartrate
            self.HBmonitor.text = [NSString stringWithFormat:@"%d BPM", aavrgHB];       //nic: Don't take average, take latest.
            self.HBmonitor.textColor = [UIColor redColor];
        }
        
    }
    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////

}
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
}
- (IBAction)toRecNextStep:(UIButton *)sender        //The Next Button
{
    //if(aHBcounter >= 2000)
    if(self.HBmonitor.textColor == [UIColor redColor])   //When the heartrate is confirmed, then ready to proceed
    {
        [self performSegueWithIdentifier:@"HBtoRecSegue" sender:self];
    }
    else
    {
        self.nextStepReadyLabel.text = @"Heart Beat has Not been Recorded...";
        [NSTimer scheduledTimerWithTimeInterval: 2.0 target: self selector: @selector(changeBackSecond:) userInfo: nil repeats: NO];
    }
}
-(void) changeBackSecond:(NSTimer*) t
{
    self.nextStepReadyLabel.text = @"After Heart Beat is Recorded, Move to the Next Step...";
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    recorderMode *controller = (recorderMode *)segue.destinationViewController;
    controller.secondOldDiscoveredPeripheral = self.oldDiscoveredPeripheral;
    controller.secondOldCentralManager = self.oldCentralManager;
    controller.myAvrgeHB = self.myAvrgeHB;
}
@end
