//
//  ViewController.m
//  blue
//
//  Created by MacMini2 on 5/20/15.
//    (c) 2015 NUS. All rights reserved.
//

#import "ViewController.h"
#import "OpenGLView.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *seizureStatus;
@property OpenGLView *cube;
@property (weak, nonatomic) IBOutlet UIView *cubeView;
@property (weak, nonatomic) IBOutlet UILabel *axView;
@property (weak, nonatomic) IBOutlet UILabel *ayView;
@property (weak, nonatomic) IBOutlet UILabel *azView;
@property (weak, nonatomic) IBOutlet UILabel *mxView;
@property (weak, nonatomic) IBOutlet UILabel *myView;
@property (weak, nonatomic) IBOutlet UILabel *mzView;
@property (weak, nonatomic) IBOutlet UILabel *gxView;
@property (weak, nonatomic) IBOutlet UILabel *gyView;
@property (weak, nonatomic) IBOutlet UILabel *gzView;
@property (weak, nonatomic) IBOutlet UILabel *qwView;
@property (weak, nonatomic) IBOutlet UILabel *qxView;
@property (weak, nonatomic) IBOutlet UILabel *qyView;
@property (weak, nonatomic) IBOutlet UILabel *qzView;
@property (weak, nonatomic) IBOutlet UILabel *timeView;
@property (weak, nonatomic) IBOutlet UILabel *yawView;
@property (weak, nonatomic) IBOutlet UILabel *pitchView;
@property (weak, nonatomic) IBOutlet UILabel *rollView;
@property (weak, nonatomic) IBOutlet UILabel *consoleView;
@property (weak, nonatomic) IBOutlet UILabel *HRView;
@property (weak, nonatomic) IBOutlet UILabel *handFreqView;
@property (weak, nonatomic) IBOutlet UILabel *startTIME;

@property BOOL handSeizureOccurs;
@property BOOL hbSeizureOccurs;
@property int seizureStartTime;
@property int seizureEndTime;

@property BOOL bluetoothOn;
@property BOOL firstPressConnectionEnd;
@property BOOL stopLoop;



//Madgwicks Stuff
#define sampleFreq	100.0f		// sample frequency in Hz
#define betaDef		0.1f
@end

@implementation ViewController
@synthesize movie;

/////////FFT//////////
//////////////////////
int counter;
int accelStoreX[256];
//////////////////////
//////////////////////
/////////HB///////////
int HBStore[2000];
int HBcounter;
int avrgHB;
//////////////////////
//////////////////////




- (void)tLog: (NSString *)msg
{
    self.consoleView.text=msg;
}
- (IBAction)startScan:(UIButton *)sender
{
    if (!self.bluetoothOn)
    {
        [self tLog:@"Bluetooth OFF"];
        return;
    }
    [self.centralManager scanForPeripheralsWithServices:nil options:nil];
}
- (IBAction)endScan:(UIButton *)sender
{
    /*
    self.axView.text = nil;
    self.ayView.text = nil;
    self.azView.text = nil;
    self.mxView.text = nil;
    self.myView.text = nil;
    self.mzView.text = nil;
    self.gxView.text = nil;
    self.gyView.text = nil;
    self.gzView.text = nil;
    self.qwView.text = nil;
    self.qxView.text = nil;
    self.qyView.text = nil;
    self.qzView.text = nil;
    self.HRView.text = nil;
    self.handFreqView.text = nil;
    self.yawView.text = nil;
    self.pitchView.text = nil;
    self.rollView.text = nil;
    self.timeView.text = nil;
     */
    self.bluetoothOn = NO;
    self.bluetoothOn = YES;
    if (!self.firstPressConnectionEnd)
    {
        [self tLog:@"Connection ended"];
        [self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
    }
    self.stopLoop = NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    //VIDEO
    NSString *video = [[NSBundle mainBundle] pathForResource:@"BLANK_SPACE" ofType:@"mp4"];
    movie = [[MPMoviePlayerController alloc]initWithContentURL:[NSURL fileURLWithPath:video]];
    
    [movie.view setFrame:CGRectMake(40, 82, 237, 133)];
    [self.view addSubview:movie.view];
    //additional customization
    movie.fullscreen = NO;
    movie.shouldAutoplay = NO;
    movie.controlStyle = MPMovieControlStyleDefault;
    ////////////////
    */
    self.cube = [[OpenGLView alloc] initWithFrame:self.cubeView.bounds];
    [self.cubeView addSubview:self.cube];
    counter = 0;
    //[self tLog:@"Bluetooth LE Device Scanner\r\n\r\nZikang Tong"];
    self.bluetoothOn = NO;
    self.stopLoop = NO;
    self.seizureStartTime = 0;
    self.handSeizureOccurs = NO;
    self.seizureStatus.text = @"SEIZURESTATUS:NO";
    self.firstPressConnectionEnd = YES;
    self.centralManager = [[CBCentralManager alloc]initWithDelegate:self queue:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if([[NSString stringWithFormat:@"%@", [advertisementData objectForKey:@"kCBAdvDataLocalName"]] isEqualToString:[NSString stringWithFormat:@"PD Logger 000000001"]])
    {
        [self tLog:[NSString stringWithFormat:@"%@, RSSI: %@", [advertisementData objectForKey:@"kCBAdvDataLocalName"], RSSI]];
        self.discoveredPeripheral = peripheral;
        [self.centralManager connectPeripheral:peripheral options:nil];
    }
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    [self tLog:@"Failed to connect"];
}
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    peripheral.delegate = self;
    [peripheral discoverServices:nil];
    [self tLog:@"Connection made"];
    self.firstPressConnectionEnd = NO;
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if(error)
    {
        [self tLog:[error description]];
        return;
    }
    for (CBService *service in peripheral.services)
    {
        //[self tLog:[NSString stringWithFormat:@"Discovered Service: %@",[service description]]];
        [peripheral discoverCharacteristics:nil forService:service];
    }
}
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if(error)
    {
        [self tLog:[error description]];
        return;
    }
    for(CBCharacteristic *characteristic in service.characteristics)
    {
        //[self tLog:[NSString stringWithFormat:@"Characteristic Found: %@",[characteristic description]]];
        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
    
    }
}
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if(!self.stopLoop)
    {
        if(error)
        {
            [self tLog:[error description]];
            return;
        }
    
        //[self tLog:[NSString stringWithFormat:@"%@",characteristic.value]];
        //self.stopLoop = YES;
    
        int LSByte, MSByte;
        
        [characteristic.value getBytes:&LSByte range:NSMakeRange(6,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(7,1)];
        int16_t ax = 0x0000;
        LSByte = LSByte&0x00FF;
        ax = ((ax | MSByte)<<8)|LSByte;
        ax = (ax&0xfff0)>>4;
        if((ax&0x0800) != 0)
        {
            ax = ax|0xf000;
        }
        
        [characteristic.value getBytes:&LSByte range:NSMakeRange(8,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(9,1)];
        int16_t ay = 0x0000;
        LSByte = LSByte&0x00FF;
        ay = ((ay | MSByte)<<8)|LSByte;
        ay = (ay&0xfff0)>>4;
        if((ay&0x0800) != 0)
        {
            ay = ay|0xf000;
        }
        
        [characteristic.value getBytes:&LSByte range:NSMakeRange(10,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(11,1)];
        int16_t az = 0x0000;
        LSByte = LSByte&0x00FF;
        az = ((az | MSByte)<<8)|LSByte;
        az = (az&0xfff0)>>4;
        if((az&0x0800) != 0)
        {
            az = az|0xf000;
        }
        
        [characteristic.value getBytes:&LSByte range:NSMakeRange(0,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(1,1)];
        int16_t mx = 0x0000;
        LSByte = LSByte&0x00FF;
        mx = ((mx | MSByte)<<8)|LSByte;
        mx = mx&0x0fff;
        if((mx&0x0800) != 0)
        {
            mx = mx|0xf000;
        }
        
        [characteristic.value getBytes:&LSByte range:NSMakeRange(2,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(3,1)];
        int16_t my = 0x0000;
        LSByte = LSByte&0x00FF;
        my = ((my | MSByte)<<8)| LSByte;
        my = my&0x0fff;
        if((my&0x0800) != 0)
        {
            my = my|0xf000;
        }
        
        [characteristic.value getBytes:&LSByte range:NSMakeRange(4,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(5,1)];
        int16_t mz = 0x0000;
        LSByte = LSByte&0x00FF;
        mz = ((mz | MSByte)<<8)| LSByte;
        mz = mz&0x0fff;
        if((mz&0x0800) != 0)
        {
            mz = mz|0xf000;
        }
        
        [characteristic.value getBytes:&LSByte range:NSMakeRange(12,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(13,1)];
        int16_t gx = 0x0000;
        LSByte = LSByte&0x00FF;
        gx = ((gx | MSByte)<<8)|LSByte;
        
        [characteristic.value getBytes:&LSByte range:NSMakeRange(14,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(15,1)];
        int16_t gy = 0x0000;
        LSByte = LSByte&0x00FF;
        gy = ((gy | MSByte)<<8)|LSByte;

        [characteristic.value getBytes:&LSByte range:NSMakeRange(16,1)];
        [characteristic.value getBytes:&MSByte range:NSMakeRange(17,1)];
        int16_t gz = 0x0000;
        LSByte = LSByte&0x00FF;
        gz = ((gz | MSByte)<<8)|LSByte;
        
        /////
        int byte1, byte2, byte3, byte4, byte5, byte6;
        [characteristic.value getBytes:&byte1 range:NSMakeRange(1, 1)];
        [characteristic.value getBytes:&byte2 range:NSMakeRange(3, 1)];
        [characteristic.value getBytes:&byte3 range:NSMakeRange(5, 1)];
        [characteristic.value getBytes:&byte4 range:NSMakeRange(6, 1)];
        [characteristic.value getBytes:&byte5 range:NSMakeRange(8, 1)];
        [characteristic.value getBytes:&byte6 range:NSMakeRange(10, 1)];
        uint32_t time = 0x00;
        uint32_t temp = 0x00;
        temp = (byte1&0xF0)>>4;
        temp = (temp & 0x0F);
        time = time | temp;
        temp = 0x00;
        temp = (byte2&0xF0)>>4;
        temp = (temp & 0x0F)<<4;
        time = time | temp;
        temp = 0x00;
        temp = (byte3&0xF0)>>4;
        temp = (temp & 0x0F)<<8;
        time = time | temp;
        temp = 0x00;
        temp = (byte4&0x0F);
        temp = (temp & 0x0F)<<12;
        time = time | temp;
        temp = 0x00;
        temp = (byte5&0x0F);
        temp = (temp & 0x0F)<<16;
        time = time | temp;
        temp = 0x00;
        temp = (byte6&0x0F);
        temp = (temp & 0x0F)<<20;
        time = time | temp;
        float time_float = time/1000;
        [characteristic.value getBytes:&LSByte range:NSMakeRange(18,1)];
        uint8_t heartrate = 0x00;
        heartrate = heartrate|LSByte;
        
    

        gx = GLKMathDegreesToRadians(gx/14.375);
        gy = GLKMathDegreesToRadians(gy/14.375);
        gz = GLKMathDegreesToRadians(gz/14.375);

        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.axView.text = [NSString stringWithFormat:@"%d",ax];
            self.ayView.text = [NSString stringWithFormat:@"%d",ay];
            self.azView.text = [NSString stringWithFormat:@"%d",az];
            self.mxView.text = [NSString stringWithFormat:@"%d",mx];
            self.myView.text = [NSString stringWithFormat:@"%d",my];
            self.mzView.text = [NSString stringWithFormat:@"%d",mz];
            self.gxView.text = [NSString stringWithFormat:@"%d",gx];
            self.gyView.text = [NSString stringWithFormat:@"%d",gy];
            self.gzView.text = [NSString stringWithFormat:@"%d",gz];
            //self.qwView.text = [NSString stringWithFormat:@"%.2f",q0];
            //self.qxView.text = [NSString stringWithFormat:@"%.2f",q1];
            //self.qyView.text = [NSString stringWithFormat:@"%.2f",q2];
            //self.qzView.text = [NSString stringWithFormat:@"%.2f",q3];
            self.HRView.text = [NSString stringWithFormat:@"%d BPM",heartrate];
            self.timeView.text = [NSString stringWithFormat:@"%.0f seconds",time_float];
        });


    
        if(counter < 256)
        {
            accelStoreX[counter] = az;
            //printf("%d\n",accelStoreX[counter]);
            counter++;
        }
        else
        {
            
            const int log2n = 8;
            const int n = 1 << log2n;
            const int nOver2 = n / 2;
            
            FFTSetupD fftSetup = vDSP_create_fftsetupD (log2n, kFFTRadix2);
            
            double* input;
            
            DSPDoubleSplitComplex fft_data;
            
            int i;
            
            input = malloc(n * sizeof(double));
            fft_data.realp = malloc(nOver2 * sizeof(double));
            fft_data.imagp = malloc(nOver2 * sizeof(double));
            
            for(int j = 0; j<n; j++)
            {
                input[j] = (double)accelStoreX[j];
            }
            
            //printf("Input\n");
            
            for (i = 0; i < n; ++i)
            {
                //printf("%d: %8g\n", i, input[i]);
            }
            
            vDSP_ctozD((DSPDoubleComplex *)input, 2, &fft_data, 1, nOver2);
            
            //printf("FFT Input\n");
            
            for (i = 0; i < nOver2; ++i)
            {
                //printf("%d: %8g%8g\n", i, fft_data.realp[i], fft_data.imagp[i]);
            }
            
            vDSP_fft_zripD (fftSetup, &fft_data, 1, log2n, kFFTDirection_Forward);
            
            //printf("FFT output\n");
            
            for (i = 0; i < nOver2; ++i)
            {
                //printf("%d: %8g%8g\n", i, fft_data.realp[i], fft_data.imagp[i]);
            }
            
            for (i = 0; i < nOver2; ++i)
            {
                fft_data.realp[i] *= 0.5;
                fft_data.imagp[i] *= 0.5;
            }
            
            //printf("Scaled FFT output\n");

            double largest = 0;
            int largestIndex = 0;
            for (i = 1; i < nOver2; i++)
            {
                //printf("%d: %8g%8g\n", i, fft_data.realp[i], fft_data.imagp[i]);
                if(largest  <  (fft_data.realp[i]*fft_data.realp[i] + fft_data.imagp[i]*fft_data.imagp[i]))
                {
                    largest = ((fft_data.realp[i]*fft_data.realp[i] + fft_data.imagp[i]*fft_data.imagp[i]));
                    largestIndex = i;
                }
            }
            if(largestIndex > 15)
            {
                largestIndex = 0;
            }
            float freq = largestIndex*sampleFreq/256;
            self.handFreqView.text = [NSString stringWithFormat:@"%.1f Hz",freq];
            
            //printf("Unpacked output\n");
            
            //printf("%d: %8g%8g\n", 0, fft_data.realp[0], 0.0); // DC
            for (i = 1; i < nOver2; ++i)
            {
                //printf("%d: %8g%8g\n", i, fft_data.realp[i], fft_data.imagp[i]);
            }
            
            //////////////////////////////////////
            //////////////////////////////////////
            //Getting Resting HB!!!!!!!!!!!!!!!!//
            //////////////////////////////////////
            //////////////////////////////////////
            if(HBcounter<2000)
            {
                if(time_float > 120)
                {
                    HBStore[HBcounter] = heartrate;
                    HBcounter++;
                }
            }
            if(HBcounter>=2000)
            {
                for(int i = 0; i<2000; i++)
                {
                    avrgHB +=HBStore[i];
                }
                avrgHB /= 2000;
            }
            
            
            
            //////////////////////////////////////
            //////////////////////////////////////
            //Check's if seizure happens!!!!!!!!//
            //////////////////////////////////////
            //////////////////////////////////////
            if(freq-2.8 <= 0.7 && freq-2.8 >= -0.7 && self.seizureStartTime == 0)
            {
                self.seizureStartTime = (int)time_float;
            }
            
            if((int)time_float-self.seizureStartTime > 20 && (int)time_float-self.seizureStartTime < 30)
            {
                if(freq-1.2 <= 0.5 && freq-1.2 >= -0.5)
                {
                    self.handSeizureOccurs = YES;
                }
            }
            if(!self.handSeizureOccurs)
            {
                self.seizureStatus.text = @"SEIZURESTATUS:NO";
            }
            else
            {
                self.seizureStatus.text = @"SEIZURESTATUS:YES";
            }
            /////////////////////////////////////
            self.startTIME.text = [NSString stringWithFormat:@"%d", self.seizureStartTime];
            counter = 0;
        }
    }
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state != CBCentralManagerStatePoweredOn)
    {
        [self tLog:@"Bluetooth OFF"];
        self.bluetoothOn = NO;
    }
    else
    {
        [self tLog:@"Bluetooth ON"];
        self.bluetoothOn = YES;
    }
    
}
@end
