#import "OpenGLView.h"
#import <GLKit/GLKit.h>

@property (weak, nonatomic) IBOutlet UIView *cubeView;
@property (strong,nonatomic) OpenGLView *cube;



-(void) viewDidAppear:(BOOL)animated
{
    //setup cubeView
    self.cube = [[OpenGLView alloc] initWithFrame:self.cubeView.bounds];
    [self.cubeView addSubview:self.cube];
}


dispatch_async(dispatch_get_main_queue(), ^{

            
            
            self.gxLabel.text = [NSString stringWithFormat:@"%d",gxShort];
            self.gyLabel.text = [NSString stringWithFormat:@"%d",gyShort];
            self.gzLabel.text = [NSString stringWithFormat:@"%d",gzShort];
            
            self.axLabel.text = [NSString stringWithFormat:@"%@",ax];
            self.ayLabel.text = [NSString stringWithFormat:@"%@",ay];
            self.azLabel.text = [NSString stringWithFormat:@"%@",az];
            
            self.mxLabel.text = [NSString stringWithFormat:@"%@",mx];
            self.myLabel.text = [NSString stringWithFormat:@"%@",my];
            self.mzLabel.text = [NSString stringWithFormat:@"%@",mz];
            
            self.timestampLabel.text = [NSString stringWithFormat:@"%@",[NSNumber numberWithUnsignedLong:ts]];
        });

      
        
        //compute quaternion
        float gyro[3];
        gyro[0] = GLKMathDegreesToRadians(gxShort/14.375);
        gyro[1] = GLKMathDegreesToRadians(gyShort/14.375);
        gyro[2] = GLKMathDegreesToRadians(gzShort/14.375);
        
        
        MadgwickAHRSupdate(-gyro[0], -gyro[1], gyro[2], (float)axShort, (float)ayShort, (float)azShort, (float)mxShort, (float)myShort, (float)mzShort);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            self.quat1Label.text = [NSString stringWithFormat:@"%f",q0]; //w
            self.quat2Label.text = [NSString stringWithFormat:@"%f",q1]; //x
            self.quat3Label.text = [NSString stringWithFormat:@"%f",q2]; //y
            self.quat4Label.text = [NSString stringWithFormat:@"%f",q3]; //z
        });
        
        //display as yaw pitch roll
        float yaw, pitch, roll;
        quatToEuler(q1,q2,q0,q3,&yaw,&pitch,&roll);       
       
        
        
        yaw = GLKMathRadiansToDegrees(yaw);
        pitch = GLKMathRadiansToDegrees(pitch);
        roll = GLKMathRadiansToDegrees(roll);
        
        self.cube.currentYaw = yaw;
        self.cube.currentPitch = pitch;
        self.cube.currentRoll = roll;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.yawOutput.text = [NSString stringWithFormat:@"%f",yaw];
            self.pitchOutput.text = [NSString stringWithFormat:@"%f",pitch];
            self.rollOutput.text = [NSString stringWithFormat:@"%f",roll];

        });
