//
//  FileUploadManager.h
//  Telerehab2
//
//  Created by Yoga on 24/2/14.
//  Copyright (c) 2014 Yoga. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUploadManager : NSObject

-(void)addPendingFileName:(NSString *)fileName;
-(void)removeUploadedFile: (NSString *)fileName;
-(BOOL) isFileUploading:(NSString *)fileName;
-(BOOL)isAnyFileUploading;


@end
