//
//  FileUploadManager.m
//  Telerehab2
//
//  Created by Yoga on 24/2/14.
//  Copyright (c) 2014 Yoga. All rights reserved.
//

#import "FileUploadManager.h"

static FileUploadManager *sharedSingleton;

@interface FileUploadManager()

@property (nonatomic, strong) NSMutableArray *listOfFilesBeingUploaded;

@end

@implementation FileUploadManager

@synthesize listOfFilesBeingUploaded = _listOfFilesBeingUploaded;

+(void)initialize{
    static BOOL initialized = NO;
    if (!initialized) {
        initialized = YES;
        sharedSingleton = [[FileUploadManager alloc] init];
    }
}

-(void)addPendingFileName:(NSString *)fileName{
    if (!sharedSingleton.listOfFilesBeingUploaded) {
        sharedSingleton.listOfFilesBeingUploaded = [NSMutableArray arrayWithCapacity:20];
    }
    [sharedSingleton.listOfFilesBeingUploaded addObject:fileName];
    
//    NSLog(@"added file %@ to the pending list", fileName);
}

-(BOOL)isFileUploading:(NSString *)fileName {
    BOOL isFileUploading =
    [sharedSingleton.listOfFilesBeingUploaded containsObject:fileName];
    
//    NSLog(@"isFound for %@ in %@ is %d",
//          fileName,
//          sharedSingleton.listOfFilesBeingUploaded,
//          isFileUploading);
    
    return isFileUploading;
}

-(BOOL)isAnyFileUploading {
    
    BOOL isAnyFileUploading = TRUE;
    if ([sharedSingleton.listOfFilesBeingUploaded count] == 0){
        isAnyFileUploading = FALSE;
    }

    
    return isAnyFileUploading;
}


-(void)removeUploadedFile: (NSString *)fileName{
    [sharedSingleton.listOfFilesBeingUploaded removeObject:fileName];
}


@end
