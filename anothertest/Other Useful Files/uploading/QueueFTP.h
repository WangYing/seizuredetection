//
//  QueueFTP.h
//  nwTest
//
//  Created by Yaadhav Raaj on 22/10/12.
//  Copyright (c) 2012 Yaadhav Raaj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"

@protocol QueueFTPDelegate<NSObject>
@required
-(void) fileUploadDone:(BOOL)success
               forFile:(NSString*)fileName;
-(void) allFilesUploaded;
//-(void) progressUpdated:(float)progress;
@end

@interface QueueFTP : NSObject{
    bool done;
    ASINetworkQueue *networkQueue;
    NSString* phpLink;
    id<QueueFTPDelegate>delegate;
    
}

@property (nonatomic,strong) ASINetworkQueue *networkQueue;
@property (nonatomic, strong) IBOutlet id <QueueFTPDelegate> delegate;
@property (nonatomic) float  uploadProgress;

-(id)init:(NSString*)_phpLink;
- (void)setDelegate:(id <QueueFTPDelegate>)aDelegate;
-(void)beginExecution;
-(void)cancelAllOperations;
-(void)addFile:(NSString*)filePath;


@end
