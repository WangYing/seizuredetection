//
//  QueueFTP.m
//  nwTest
//
//  Created by Yaadhav Raaj on 22/10/12.
//  Copyright (c) 2012 Yaadhav Raaj. All rights reserved.
//

#import "QueueFTP.h"
#import "ASIFormDataRequest.h"

@implementation QueueFTP
@synthesize networkQueue,delegate;

-(id)init:(NSString*)_phpLink{
    phpLink=_phpLink;
    
    // Stop anything already in the queue before removing it
	[[self networkQueue] cancelAllOperations];
	
	// Creating a new queue each time we use it means we don't have to worry about clearing delegates or resetting progress tracking
	[self setNetworkQueue:[ASINetworkQueue queue]];
	[[self networkQueue] setDelegate:self];
	[[self networkQueue] setRequestDidFinishSelector:@selector(requestFinished:)];
	[[self networkQueue] setRequestDidFailSelector:@selector(requestFailed:)];
	[[self networkQueue] setQueueDidFinishSelector:@selector(queueFinished:)];
    
    [self networkQueue].delegate = self;
    [[self networkQueue] setShowAccurateProgress:YES];
    
    return self;
}

-(void)addFile:(NSString*)filePath{
    NSURL* url=[[NSURL alloc]initWithString:phpLink];
    ASIFormDataRequest* request=[[ASIFormDataRequest alloc]initWithURL:url];
    [request setRequestMethod:@"POST"];
    [request setFile:filePath forKey:@"userfile"];
    NSString *fileName = [[NSURL URLWithString:filePath] lastPathComponent];
    [request setFile:filePath withFileName:fileName andContentType:nil forKey:@"userfile"];
    
    [request setUploadProgressDelegate:self];
    [request setShowAccurateProgress:YES];
    
    request.showAccurateProgress=YES;
    request.timeOutSeconds = 100;
    [request setShouldStreamPostDataFromDisk:YES];
    request.responseEncoding=NSUTF8StringEncoding;
    //NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    
    NSLog(@"FTP request added to upload file: %@", filePath);
    [[self networkQueue] addOperation:request];
}

-(void)beginExecution{
    //NSLog(@"Begin Execution called");
    
    [[self networkQueue] go];
}

-(void)cancelAllOperations{
    [[self networkQueue] cancelAllOperations];
}

- (void)requestFinished:(ASIHTTPRequest *)request       //When the uploading is finished...
{
    
	// You could release the queue here if you wanted
	if ([[self networkQueue] requestsCount] == 0) {
        
		// Since this is a retained property, setting it to nil will release it
		// This is the safest way to handle releasing things - most of the time you only ever need to release in your accessors
		// And if you an Objective-C 2.0 property for the queue (as in this example) the accessor is generated automatically for you
		[self setNetworkQueue:nil];
	}
    
    NSString* fileName;
    @try{
        
        NSString* responseStringX=[request responseString];
        
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:[responseStringX dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:nil];
        NSLog(@"Filename: %@",[jsonResponse objectForKey:@"filename"]);
        fileName=[jsonResponse objectForKey:@"filename"];
    }
    @catch (NSException* e) {
        [[self delegate]fileUploadDone:NO forFile:@"failed"];
        return;
    }
    
    NSLog(@"Finished %d ==> %@", request.responseStatusCode, [request responseString]);
    
    if([fileName isEqualToString:@""]){
        done=NO;
        [[self delegate]fileUploadDone:NO forFile:@"failed"];
    }else{
        done=YES;
        [[self delegate]fileUploadDone:YES forFile:fileName];
    }
	
	//... Handle success
	//NSLog(@"Request finished");
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
	// You could release the queue here if you wanted
	if ([[self networkQueue] requestsCount] == 0) {
		[self setNetworkQueue:nil];
	}
	
	//... Handle failure
	NSLog(@"Request failed");
}


- (void)queueFinished:(ASINetworkQueue *)queue
{
	// You could release the queue here if you wanted
	if ([[self networkQueue] requestsCount] == 0) {
		[self setNetworkQueue:nil];
	}
    
    [[self delegate]allFilesUploaded];
	//NSLog(@"Queue finished");
}

- (void)requestStarted:(ASIHTTPRequest *)request{
    //NSLog(@"req started");
}

- (void)setProgress:(float)progress
{
    //[[self delegate]progressUpdated:progress];
   NSLog(@"Upload progress %f, bytes to upload: %llu, uploaded: %llu",progress,self.networkQueue.totalBytesToUpload,self.networkQueue.bytesUploadedSoFar);
    _uploadProgress=progress;
}

- (void)setDelegate:(id <QueueFTPDelegate>)aDelegate {
    if (delegate != aDelegate) {
        delegate = aDelegate;
    }
}


@end

