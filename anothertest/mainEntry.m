//
//  mainEntry.m
//  anotherTest
//
//  Created by nus on 6/23/15.
//  Copyright (c) 2015 NUS. All rights reserved.
//

#import "mainEntry.h"
#import "HBStarter.h"
#import "recorderMode.h"

@interface mainEntry ()

@property (weak, nonatomic) IBOutlet UIButton *nextStep;
@property (weak, nonatomic) IBOutlet UILabel *nextStepLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iPadBluetoothIcon;
@property (weak, nonatomic) IBOutlet UIImageView *transferIcon;
@property (weak, nonatomic) IBOutlet UIImageView *deviceBluetoothIcon;

@property CBCentralManager *centralManager;
@property CBPeripheral *discoveredPeripheral;
@property BOOL bluetoothOn;

@property (strong,nonatomic) recorderMode *recorderModeViewController;

@end

@implementation mainEntry



-(void) callAfterThreeSecond:(NSTimer*) t
{
    [self.centralManager stopScan];
    if(self.discoveredPeripheral == nil)
    {
        //self.connectionStatus.text = @"No Beacons Available for Connection";
    }
}

- (IBAction)nextStepButton:(UIButton *)sender
{
    if(self.discoveredPeripheral == nil)
    {
        self.nextStepLabel.text = @"You are Not Connected...";
        [NSTimer scheduledTimerWithTimeInterval: 2.0 target: self selector: @selector(changeBack:) userInfo: nil repeats: NO];
    }
    else
    {
        [self performSegueWithIdentifier:@"mainToRecorderSegue" sender:self];
    }
}
-(void) changeBack:(NSTimer*) t
{
    self.nextStepLabel.text = @"Switch on the Bluetooth Device";
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.bluetoothOn = NO;
    self.centralManager = [[CBCentralManager alloc]initWithDelegate:self queue:nil];        //initialise with self delegate
    
    
    UIEdgeInsets insets = UIEdgeInsetsMake(18, 18, 18, 18);
    UIImage *nextStepImage = [UIImage imageNamed:@"blueButton@2x.png"];
    UIImage *stretchableNextStepImage = [nextStepImage resizableImageWithCapInsets:insets];
    [self.nextStep setBackgroundImage:stretchableNextStepImage forState:UIControlStateNormal];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state != CBCentralManagerStatePoweredOn)
    {
        self.bluetoothOn = NO;
        [self.iPadBluetoothIcon setImage:[UIImage imageNamed:@"Bluetooth_off"]];
        return;
        
    }
    else
    {
        [self.centralManager scanForPeripheralsWithServices:nil options:nil];
        self.bluetoothOn = YES;
        [self.iPadBluetoothIcon setImage:[UIImage imageNamed:@"Bluetooth_on"]];
        return;
        
    }
    
}

//What you do when you discover other Bluetooth devices
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if([[NSString stringWithFormat:@"%@", [advertisementData objectForKey:@"kCBAdvDataLocalName"]] isEqualToString:[NSString stringWithFormat:@"PD Logger 000000001"]])
    {
        [self.deviceBluetoothIcon setImage:[UIImage imageNamed:@"Bluetooth_on"]];
        self.discoveredPeripheral = peripheral;
        [self.centralManager connectPeripheral:peripheral options:nil];
    }
}
- (void)centralManager:(CBCentralManager *)central didFailToPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    //self.connectionStatus.text = @"Failed to Connect";

}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Disconnected");
    if (self.recorderModeViewController.isRecording)
    {
        [self.recorderModeViewController recordPress:self.recorderModeViewController.recordButton];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        //do nothing
        //need to try to implement stop movie recording here probably.
        self.recorderModeViewController = nil;      //So that the program will de-allocate the viewcontroller
    }];
    
    [self.transferIcon setImage:[UIImage imageNamed:@"transfer_off"]];
    
    [self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
    self.discoveredPeripheral = nil;
    self.nextStepLabel.text = @"Switch on the Bluetooth device";

    [self.deviceBluetoothIcon setImage:[UIImage imageNamed:@"Bluetooth_off"]];
    [self.centralManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    peripheral.delegate = self;
    [self.transferIcon setImage:[UIImage imageNamed:@"transfer_on"]];
    self.nextStepLabel.text = @"Press the Next Button";
    [self.centralManager stopScan];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    self.recorderModeViewController = (recorderMode *) segue.destinationViewController;
    self.recorderModeViewController.secondOldDiscoveredPeripheral = self.discoveredPeripheral;
    self.recorderModeViewController.secondOldCentralManager = self.centralManager;
}
@end
