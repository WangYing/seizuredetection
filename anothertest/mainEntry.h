//
//  mainEntry.h
//  anotherTest
//
//  Created by nus on 6/23/15.
//  Copyright (c) 2015 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#include <Accelerate/Accelerate.h>
@import GLKit;
@import OpenGLES;

@interface mainEntry : UIViewController

<CBCentralManagerDelegate,CBPeripheralDelegate>

#define SERVICE_UUID    @"FFF0";
#define CHARACTERISTIC_UUID    @"FFF1";
@end
