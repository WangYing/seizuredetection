//
//  recorderMode.m
//  anotherTest
//
//  Created by nus on 6/24/15.
//  Copyright (c) 2015 NUS. All rights reserved.
//

#import "recorderMode.h"
#import "ViewController.h"
#import "OpenGLView.h"
#import "PCLineChartView.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "QueueFTP.h"
#import "FileUploadManager.h"


@interface recorderMode ()
{
    NSMutableArray *_response;       //for uploading
    NSMutableData *_ResponseData;   //for SQL request
    AVCaptureDeviceInput *frontFacingCameraDeviceInput;
    AVCaptureDeviceInput *backFacingCameraDeviceInput;
    AVCaptureInput *frontFacingCameraInput;
    AVCaptureInput *backFacingCameraInput;
}



@property (weak, nonatomic) IBOutlet UIView *cubeView;
@property OpenGLView *cube;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet UIView *valuesView;

@property (strong, nonatomic) AVCaptureSession *avSession;
@property AVCaptureDevice *audioDevice;
@property AVCaptureDevice *videoDevice;
@property AVCaptureDeviceInput *audioInput;
@property AVCaptureDeviceInput *videoInput;
@property AVCaptureMovieFileOutput *recordingOutput;
@property AVCaptureVideoPreviewLayer *previewLayer;
@property CALayer *rootLayer;
@property BOOL isFrontCameraSelected;

@property (strong,atomic)NSString *xmlURL;
@property (strong,nonatomic)NSString *outPutFileName;

@property (strong, nonatomic) IBOutlet UIImageView *cameraRedDot;
@property (strong, nonatomic) IBOutlet UIButton *viewDataButton;
@property (strong, nonatomic) IBOutlet UIButton *heartGraphButton;
@property (strong, nonatomic) IBOutlet UIButton *valuesButton;


- (IBAction)switchCameraTapped:(id)sender;
@property BOOL backToFrontFirstEntry;
@property BOOL frontToBackFirstEntry;


@property BOOL startingTimeStampBOOL;
@property BOOL timeOverflow;
@property int startingTimeStamp;
@property int timeStamp_Prev;
@property int seizureStartTime;
@property BOOL handSeizureOccurs;
@property BOOL everyOther;
@property NSMutableArray *graphStorage;
@property NSMutableArray *dataDict;
@property NSString *dateString;
@property (strong,nonatomic)AFHTTPRequestOperationManager *manager;

@property (weak, nonatomic) IBOutlet UILabel *axLabel;
@property (weak, nonatomic) IBOutlet UILabel *ayLabel;
@property (weak, nonatomic) IBOutlet UILabel *azLabel;
@property (weak, nonatomic) IBOutlet UILabel *mxLabel;
@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@property (weak, nonatomic) IBOutlet UILabel *mzLabel;
@property (weak, nonatomic) IBOutlet UILabel *gxLabel;
@property (weak, nonatomic) IBOutlet UILabel *gyLabel;
@property (weak, nonatomic) IBOutlet UILabel *gzLabel;
@property (weak, nonatomic) IBOutlet UILabel *qwLabel;
@property (weak, nonatomic) IBOutlet UILabel *qxLabel;
@property (weak, nonatomic) IBOutlet UILabel *qyLabel;
@property (weak, nonatomic) IBOutlet UILabel *qzLabel;
@property (weak, nonatomic) IBOutlet UILabel *yawLabel;
@property (weak, nonatomic) IBOutlet UILabel *pitchLabel;
@property (weak, nonatomic) IBOutlet UILabel *rollLabel;
@property (weak, nonatomic) IBOutlet UILabel *heartbeatLabel;
@property (weak, nonatomic) IBOutlet UILabel *handFreqLabel;
@property (weak, nonatomic) IBOutlet UILabel *timestampLabel;
@property (weak, nonatomic) IBOutlet UILabel *recStartTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *recStopTimeLabel;
@property (strong, atomic)NSString *mainURL;

@property (weak, nonatomic) IBOutlet UIImageView *heartImage;
@property (weak, nonatomic) IBOutlet UIImageView *vibrationImage;
- (IBAction)uploadVideoSwitchValueChanged:(UISwitch *)sender;
@property BOOL uploadVideoSwitchOn;

@property NSFileHandle* sensorLogFileHandle;

@property (nonatomic, strong) QueueFTP *queueFTP;
@property (nonatomic, strong) FileUploadManager *fileUploadManager;

//Madgwicks Stuff
#define sampleFreq	100.0f		// sample frequency in Hz
#define betaDef		0.1f
@end

@implementation recorderMode
/////////Graph////////
int HBIndivStorage[30];
int HBIndivStorageCounter;
int HBAvrge;
//////////////////////
//////////////////////
/////////FFT//////////
//////////////////////
int FFTcounter;
int accelStore[256];
//////////////////////
//////////////////////
int maxSizeOfDict = 10000;
int dictSizeCounter = 0;
//////////////////////
BOOL sixtotwelvesec = false;
int sixtotwelvetime;


/////CHECK!!!!!!!
bool doOnce = YES;




- (IBAction)viewData:(UIButton *)sender
{
    self.dataView.hidden = YES;
    self.cubeView.hidden = NO;
    self.valuesView.hidden = YES;
}
- (IBAction)viewHeartChart:(UIButton *)sender
{
    self.dataView.hidden = NO;
    self.cubeView.hidden = YES;
    self.valuesView.hidden = YES;
}
- (IBAction)viewValues:(UIButton *)sender
{
    self.dataView.hidden = YES;
    self.cubeView.hidden = YES;
    self.valuesView.hidden = NO;
}
-(NSDictionary *)getGraphData:(NSMutableArray *)datapoints
{
    NSMutableArray *data = [[NSMutableArray alloc]init];
    NSMutableArray *points = [[NSMutableArray alloc]init];
    NSMutableArray *xLabels = [[NSMutableArray alloc]init];
    
    for(int i = 1; i<=10; i++)
    {
        for(int j = 0; j<10; j++)
        {
            [points addObject:[datapoints objectAtIndex:(NSInteger)j]];
        }
        NSDictionary *graphInfo = @{@"data":[points mutableCopy]};
        [data addObject:graphInfo];
    }
    for(int j = 10; j>0; j--)
    {
        [xLabels addObject:[NSString stringWithFormat:@"-%.1f", (float)j/10]];
    }
    NSDictionary *results = @{@"data":data,@"xLabels":xLabels};
    return results;
}



- (void)viewDidLoad
{
    [super viewDidLoad];

 
    self.frontToBackFirstEntry = TRUE;
    self.backToFrontFirstEntry = TRUE;
    self.isFrontCameraSelected = FALSE;     //starting from back camera
    
    self.startingTimeStampBOOL = YES;
    self.timeOverflow = NO;
    self.timeStamp_Prev = 0;
    FFTcounter = 0;
    self.seizureStartTime = 0;
    self.handSeizureOccurs = NO;
    self.everyOther = NO;
    self.graphStorage = [[NSMutableArray alloc]init];
    HBIndivStorageCounter = 0;
    HBAvrge = 0;
    self.dataDict = [[NSMutableArray alloc] init];
    self.manager = [AFHTTPRequestOperationManager manager];
    self.dateString = [[NSString alloc] init];
    
    ////Graph Starting
    for(int i = 0; i<10; i++)
    {
        [self.graphStorage addObject:[NSNumber numberWithInt:self.myAvrgeHB]];
    }
    PCLineChartView *lineGraph = [[PCLineChartView alloc]initWithFrame:CGRectMake(10,10, 700, 400)];
    [lineGraph setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    lineGraph.minValue = 0;
    lineGraph.maxValue = 200;
    [self.dataView addSubview:lineGraph];
    
    NSMutableArray *components = [NSMutableArray array];
    NSDictionary *results = [self getGraphData:self.graphStorage];
    NSMutableArray *data = [results valueForKey:@"data"];
    
    for(NSDictionary *graphInfo in data)
    {
        PCLineChartViewComponent *component = [[PCLineChartViewComponent alloc]init];
        [component setTitle:graphInfo[@"title"]];
        [component setPoints:graphInfo[@"data"]];
        [component setShouldLabelValues:NO];
        [component setColour:PCColorBlue];
        [components addObject:component];
    }
    [lineGraph setComponents:components];
    [lineGraph setXLabels:[results valueForKey:@"xLabels"]];
        
    
    //make views look cool
    self.cameraView.layer.borderColor = [UIColor blackColor].CGColor;
    self.cameraView.layer.borderWidth = 3.0f;
    self.dataView.layer.borderColor = [UIColor blackColor].CGColor;
    self.dataView.layer.borderWidth = 3.0f;
    self.dataView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.cubeView.layer.borderColor = [UIColor blackColor].CGColor;
    self.cubeView.layer.borderWidth = 3.0f;
    self.valuesView.layer.borderColor = [UIColor blackColor].CGColor;
    self.valuesView.layer.borderWidth = 3.0f;
     
    
    //View Data button make cool
    UIImage *viewDataButtonImage = [UIImage imageNamed:@"blueButton@2x.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(18, 18, 18, 18);
    UIImage *stretchableViewDataButtonImage = [viewDataButtonImage resizableImageWithCapInsets:insets];
    [self.viewDataButton setBackgroundImage:stretchableViewDataButtonImage forState:UIControlStateNormal];
    UIImage *heartGraphButtonImage = [UIImage imageNamed:@"orangeButton@2x.png"];
    UIImage *stretchableHeartGraphButtonImage = [heartGraphButtonImage resizableImageWithCapInsets:insets];
    [self.heartGraphButton setBackgroundImage:stretchableHeartGraphButtonImage forState:UIControlStateNormal];
    UIImage *valuesButtonImage = [UIImage imageNamed:@"greenButton@2x.png"];
    UIImage *stretchableValuesButtonImage = [valuesButtonImage resizableImageWithCapInsets:insets];
    [self.valuesButton setBackgroundImage:stretchableValuesButtonImage forState:UIControlStateNormal];
    
    self.secondOldDiscoveredPeripheral.delegate = self;
    [self.secondOldDiscoveredPeripheral discoverServices:nil];        //changed by Nic
    self.isRecording = NO;
    
    //initialize cube
    self.cube = [[OpenGLView alloc] initWithFrame:self.cubeView.bounds];
    [self.cubeView addSubview:self.cube];
    
    //initialize camera
    NSError *error;
    self.avSession = [[AVCaptureSession alloc] init];
    [self.avSession setSessionPreset:AVCaptureSessionPresetMedium];     //Video quality set to Medium instead of High
    
    //set audio device
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera;
    AVCaptureDevice *backCamera;
    
    //Choose which camera to use....
    for (AVCaptureDevice *device in devices) {
        if ([device hasMediaType:AVMediaTypeVideo]) {
            if([device position] == AVCaptureDevicePositionBack) {
                backCamera = device;
            }
            else {
                frontCamera = device;
            }
        }
        
        else if ([device hasMediaType:AVMediaTypeAudio]) {
            self.audioDevice = device;
            //NSLog(@"Device added to audioDevice: %@",device);
        }
    }
    //self.videoDevice = frontCamera;
    self.videoDevice = backCamera;      //change to back camera instead
    
    //add to input device to device input
    self.videoInput = [AVCaptureDeviceInput deviceInputWithDevice:self.videoDevice error:&error];
    self.audioInput = [AVCaptureDeviceInput deviceInputWithDevice:self.audioDevice error:&error];
    if([self.avSession canAddInput:self.videoInput])
        [self.avSession addInput:self.videoInput];
    else
        NSLog(@"Error adding video input");
    if([self.avSession canAddInput:self.audioInput])
        [self.avSession addInput:self.audioInput];
    else
        NSLog(@"Error adding audio input");
    
    //configure to 720p60fps: 13 for ipad mini
    if ([self.videoDevice lockForConfiguration:nil]) {
        //NSLog(@"%@",self.videoDevice.formats); //list of formats available
        AVCaptureDeviceFormat* currdf = [self.videoDevice.formats objectAtIndex:8];
        self.videoDevice.activeFormat = currdf;
        
        int maxSupportedFrameRate = [[self.videoDevice.activeFormat.videoSupportedFrameRateRanges objectAtIndex:0] minFrameRate]; //Changed to minimum**
        //NSLog(@"Max Support Frame Rate: %d",maxSupportedFrameRate);
        self.videoDevice.activeVideoMaxFrameDuration = CMTimeMake(1, maxSupportedFrameRate);
        
        self.videoDevice.videoZoomFactor = 1.0;
        
        [self.videoDevice unlockForConfiguration];
        //NSLog(@"Camera config: %@",self.videoDevice.activeFormat);
    }
    else {
        NSLog(@"Failed to configure camera, camera config: %@",self.videoDevice.activeFormat);
    }
    
    //for video saving
    self.recordingOutput = [[AVCaptureMovieFileOutput alloc] init];
    if([self.avSession canAddOutput:self.recordingOutput])
        [self.avSession addOutput:self.recordingOutput];

    //migrated from previous ViewDidAppear
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.avSession];
    [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    self.rootLayer = [[self cameraView] layer];
    [self.rootLayer setMasksToBounds:YES];
    [self.previewLayer setFrame:CGRectMake(0,0,736,425)];
    [self.rootLayer insertSublayer:self.previewLayer atIndex:0];
    
    [self.avSession startRunning];
    
    
    self.uploadVideoSwitchOn = YES;     //initialise its original value
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];   //prevent the app from closing.
}

- (IBAction)recordPress:(UIButton *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [NSTimeZone localTimeZone];
    [dateFormatter setDateFormat:@"hh:mm:ss a"];
    
    if(!self.isRecording)
    {
        self.recStartTimeLabel.text = [dateFormatter stringFromDate:[NSDate date]];
        [dateFormatter setDateFormat:@"dd_MM_yyyy_HH_mm_ss"];   //NOTE: Change formatting of date!
        self.dateString = [dateFormatter stringFromDate:[NSDate date]];     //set the current time and date of the recording as when it actually STARTS.
        
        UIImage *image = [UIImage imageNamed: @"red-dot-hi.png"];
        [self startRecording];
        self.isRecording = YES;
        [self.cameraRedDot setImage:image];
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];   //prevent iPad from sleeping
        [[UIScreen mainScreen] setBrightness:0.5];      //set the brightness to 50%.
        

    }
    else
    {
        self.recStopTimeLabel.text = [dateFormatter stringFromDate:[NSDate date]];
//        NSString *csvDataString = [[NSString alloc] init];
        
//        csvDataString = [[self.dataDict valueForKey:@"description"] componentsJoinedByString:@"\n"];
//        csvDataString = [@"ax,ay,az,mx,my,mz,gx,gy,gz,HeartRate,Timestamp\n" stringByAppendingString:csvDataString];
//        
        //NSString *dateInString = [NSString stringWithFormat:@"/data_%@.csv", self.dateString];     //prepare for the CSV version of format
        //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
        [self stopRecording];   //Do the CSV files first, in case the App crashes while stopping the video recording
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];   //allow iPad to sleeping once again
        [self uploadDataAndMovie];
        
        //dictSizeCounter = 0;
        self.timeOverflow = NO;     //reset overflow flag
        
        //NSString *filePath = [documentsDirectory stringByAppendingString:dateInString];
        
        
        //COMMENTED FOR NOW
        
//        NSError *csvError = NULL;
//        BOOL written = [csvDataString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&csvError];
//        
//        
//        //If there was a problem saving we show the error if not show success and file path
//        if (!written)
//            NSLog(@"write failed, error=%@", csvError);
//        else
//            NSLog(@"Saved! File path =%@", filePath);
//        [self.dataDict removeAllObjects];
        
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark AV Session Delegates and functions
-(void)startRecording
{
    NSURL *fileURL = [self getURLforVideoRecording];
    [self.recordingOutput startRecordingToOutputFileURL:fileURL recordingDelegate:self];
}

-(NSURL*)getURLforVideoRecording{
    
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [searchPaths objectAtIndex:0];
    //NSLog(@"%@",documentPath);
    //double currentTime = [[NSDate date] timeIntervalSince1970]; //just to make the files unique
    //[NSTimeZone localTimeZone];
    

    
    //make sure first letter is not capped for server code compliance
    
    NSString *recordingFileName;
    if (self.uploadVideoSwitchOn) {
        recordingFileName = [NSString stringWithFormat:@"MOV_%@",self.dateString];}
    else {
        recordingFileName = [NSString stringWithFormat:@"MOVx_%@",self.dateString];}
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.mov",documentPath,recordingFileName]];       //saving at the document directory
    
    return url;
    
}

/*

// For responding to the user accepting a newly-captured picture or movie
- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToSave;
    
    // Handle a still image capture
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            imageToSave = editedImage;
        } else {
            imageToSave = originalImage;
        }
        
        // Save the new image (original or edited) to the Camera Roll
        UIImageWriteToSavedPhotosAlbum (imageToSave, nil, nil , nil);
    }
    
    // Handle a movie capture
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeMovie, 0)
        == kCFCompareEqualTo) {
        
        NSString *moviePath = [[info objectForKey:
                                UIImagePickerControllerMediaURL] path];
        
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
            UISaveVideoAtPathToSavedPhotosAlbum (
                                                 moviePath, nil, nil, nil);
        }
    }
}
*/

-(void)stopRecording
{
    self.isRecording = NO;
    [self.cameraRedDot setImage:nil];
    self.recordButton.highlighted = NO;
    self.recordButton.selected = NO;
    [self.recordingOutput stopRecording];
    
    /*
        [newLibrary writeVideoAtPathToSavedPhotosAlbum:videoURL completionBlock:^(NSURL *assetURL, NSError *error) {
        if (error) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error!"
                                                          message:@"Video Saving Failed!"
                                                         delegate: self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
            NSLog(@"error description: %@", [error localizedDescription]);
            
            [alert show];
        }
        else{
            UIAlertView  *alert =[[UIAlertView alloc] initWithTitle:@"Video saved"
                                                            message: @"Saved to Photo Album"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        }
    }];
     */    // temporalily disable save to camera roll
    
}
-(void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    NSLog(@"Recording to file ended for %@",outputFileURL);
    NSString* folderpath;
    
    NSScanner* folderPathScanner = [NSScanner scannerWithString:[outputFileURL absoluteString]];
    while ([folderPathScanner isAtEnd] == NO){
        [folderPathScanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@"/"] intoString:NULL];
        [folderPathScanner setScanLocation: [folderPathScanner scanLocation]+2];
        if ([folderPathScanner scanUpToString:@"Documents/" intoString:&folderpath])
            break;
        
    }
    folderpath = [NSString stringWithFormat:@"%@Documents/", folderpath];
    //NSLog(@"%@", folderpath);
    
    
    //save raw data
    
    //NSString *haystack = [outputFileURL absoluteString];
    //NSString *prefix = @"file:///"; // string prefix, not needle prefix!
    //NSArray *components = [haystack componentsSeparatedByString:@"/"];
    //NSString *substring = [components objectAtIndex:8]; // the third item in the array
    //NSRange needleRange = NSMakeRange(prefix.length,
                                      //haystack.length - prefix.length-substring.length);
    //NSString *needle = [haystack substringWithRange:needleRange];
}

#pragma mark - Bluetooth Central Manager
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
}


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if(error)
    {
        NSLog(@"%@",[error description]);
        return;
    }
    for (CBService *service in peripheral.services)
    {
        //NSLog(@"Discovered Service: %@",[service description]);
        [peripheral discoverCharacteristics:nil forService:service];
    }
}
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if(error)
    {
        NSLog(@"%@",[error description]);
        return;
    }
    for(CBCharacteristic *characteristic in service.characteristics)
    {
        //[self tLog:[NSString stringWithFormat:@"Characteristic Found: %@",[characteristic description]]];
        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        
    }
}


- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if(self.isRecording)
    {
        self.recordButton.highlighted = YES;
    }
    int LSByte, MSByte;
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(6,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(7,1)];
    int16_t ax = 0x0000;
    LSByte = LSByte&0x00FF;
    ax = ((ax | MSByte)<<8)|LSByte;
    ax = (ax&0xfff0)>>4;
    if((ax&0x0800) != 0)
    {
        ax = ax|0xf000;                                                    //aX
    }
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(8,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(9,1)];
    int16_t ay = 0x0000;
    LSByte = LSByte&0x00FF;
    ay = ((ay | MSByte)<<8)|LSByte;
    ay = (ay&0xfff0)>>4;
    if((ay&0x0800) != 0)
    {
        ay = ay|0xf000;                                                    //aY
    }
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(10,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(11,1)];
    int16_t az = 0x0000;
    LSByte = LSByte&0x00FF;
    az = ((az | MSByte)<<8)|LSByte;
    az = (az&0xfff0)>>4;
    if((az&0x0800) != 0)
    {
        az = az|0xf000;                                                    //aZ
    }
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(0,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(1,1)];
    int16_t mx = 0x0000;
    LSByte = LSByte&0x00FF;
    mx = ((mx | MSByte)<<8)|LSByte;
    mx = mx&0x0fff;
    if((mx&0x0800) != 0)
    {
        mx = mx|0xf000;                                                     //mX
    }
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(2,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(3,1)];
    int16_t my = 0x0000;
    LSByte = LSByte&0x00FF;
    my = ((my | MSByte)<<8)| LSByte;
    my = my&0x0fff;
    if((my&0x0800) != 0)
    {
        my = my|0xf000;                                                     //mY
    }
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(4,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(5,1)];
    int16_t mz = 0x0000;
    LSByte = LSByte&0x00FF;
    mz = ((mz | MSByte)<<8)| LSByte;
    mz = mz&0x0fff;
    if((mz&0x0800) != 0)
    {
        mz = mz|0xf000;                                                     //mZ
    }
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(12,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(13,1)];
    int16_t gx = 0x0000;
    LSByte = LSByte&0x00FF;
    gx = ((gx | MSByte)<<8)|LSByte;                                         //gX
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(14,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(15,1)];
    int16_t gy = 0x0000;
    LSByte = LSByte&0x00FF;
    gy = ((gy | MSByte)<<8)|LSByte;                                         //gY
    
    [characteristic.value getBytes:&LSByte range:NSMakeRange(16,1)];
    [characteristic.value getBytes:&MSByte range:NSMakeRange(17,1)];
    int16_t gz = 0x0000;
    LSByte = LSByte&0x00FF;
    gz = ((gz | MSByte)<<8)|LSByte;                                         //gZ
    
    /////
    int byte1, byte2, byte3, byte4, byte5, byte6;
    [characteristic.value getBytes:&byte1 range:NSMakeRange(1, 1)];
    [characteristic.value getBytes:&byte2 range:NSMakeRange(3, 1)];
    [characteristic.value getBytes:&byte3 range:NSMakeRange(5, 1)];
    [characteristic.value getBytes:&byte4 range:NSMakeRange(6, 1)];
    [characteristic.value getBytes:&byte5 range:NSMakeRange(8, 1)];
    [characteristic.value getBytes:&byte6 range:NSMakeRange(10, 1)];
    uint32_t time = 0x00;
    uint32_t temp = 0x00;
    temp = (byte1&0xF0)>>4;
    temp = (temp & 0x0F);       //keep bits 0:3 only.
    time = time | temp;
    temp = 0x00;
    temp = (byte2&0xF0)>>4;
    temp = (temp & 0x0F)<<4;    //keep bits 4:7 only.
    time = time | temp;
    temp = 0x00;
    temp = (byte3&0xF0)>>4;
    temp = (temp & 0x0F)<<8;    //keep bits 8:11 only.
    time = time | temp;
    temp = 0x00;
    temp = (byte4&0x0F);
    temp = (temp & 0x0F)<<12;   //keep bits 12:15 onlu.
    time = time | temp;
    temp = 0x00;
    temp = (byte5&0x0F);
    temp = (temp & 0x0F)<<16;
    time = time | temp;
    temp = 0x00;
    temp = (byte6&0x0F);
    temp = (temp & 0x0F)<<20;
    time = time | temp;     //NOTE that the variable <time> maximum is 24-bits.

    int timestamp = (int)time;
    
    if (timestamp < self.timeStamp_Prev)       //If the previous timestamp is > the current one, overflow happened
    {
        if (!self.timeOverflow)  //only happens when timestamp did not overflow (default)
        {
            self.timeOverflow = YES;
        }
    }
    if (self.timeOverflow)       //After timestamp overflowed, always add the previous timestamp and the current.
    {
        timestamp = timestamp + self.timeStamp_Prev;
    }
    else    //While overflow haven't happen, keep track of previous value of timestamp
    {
        self.timeStamp_Prev = timestamp;
    }
    float time_float = (float)timestamp/1000.0;

    [characteristic.value getBytes:&LSByte range:NSMakeRange(18,1)];
    uint8_t heartrate = 0x00;
    heartrate = heartrate|LSByte;
    
    float gyro[3];
    
    
    gyro[0] = GLKMathDegreesToRadians((float)gx/14.375);
    gyro[1] = GLKMathDegreesToRadians((float)gy/14.375);
    gyro[2] = GLKMathDegreesToRadians((float)gz/14.375);
    
    MadgwickAHRSupdate(gyro[0], gyro[1], gyro[2], (float)ax, (float)ay, (float)az, (float)mx, (float)my, (float)mz);
    
    float yaw, pitch, roll;
    yaw = atan2(2*q2*q0-2*q1*q3 , 1 - 2*q2*q2 - 2*q3*q3);
    pitch = asin(2*q1*q2 + 2*q3*q0);
    roll = atan2(2*q1*q0-2*q2*q3 , 1 - 2*q1*q1 - 2*q3*q3);
    
    yaw = GLKMathRadiansToDegrees(yaw);
    pitch = GLKMathRadiansToDegrees(pitch);
    roll = GLKMathRadiansToDegrees(roll);
    
    self.cube.currentYaw = pitch; //this sensor has different reference frame than the madgwick algorithm so coordinates change!!!
    self.cube.currentPitch = yaw;
    self.cube.currentRoll = roll;
    
    //HB graphing
    if([self.graphStorage count] < 10)
    {
        //When less than 30 heartrate readings
        if(HBIndivStorageCounter < 30)
        {
            HBIndivStorage[HBIndivStorageCounter] = heartrate;
            HBIndivStorageCounter++;
        }
        //When more than 30 heartrate readings...
        else
        {
            for(int i = 0; i<30; i++)       //Add up all the heartrate readings together
            {
                HBAvrge += HBIndivStorage[i];
            }
            HBAvrge /= 30;      //Get the average heartrate reading
            
            [self.graphStorage addObject:[NSNumber numberWithInt:HBAvrge]];
            
            HBAvrge = 0;
            HBIndivStorageCounter = 0;
        }
    }
    else        //When [graphstorage count] > 10
    {
        NSArray *viewsToRemove = [self.dataView subviews];
        for (UIView *v in viewsToRemove)
        {
            [v removeFromSuperview];
        }
        
        PCLineChartView *lineGraph = [[PCLineChartView alloc]initWithFrame:CGRectMake(10,10, 700, 400)];
        [lineGraph setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        lineGraph.minValue = 0;
        lineGraph.maxValue = 200;
        [self.dataView addSubview:lineGraph];

        NSMutableArray *components = [NSMutableArray array];
        NSDictionary *results = [self getGraphData:self.graphStorage];
        NSMutableArray *data = [results valueForKey:@"data"];
    
        for(NSDictionary *graphInfo in data)
        {
            PCLineChartViewComponent *component = [[PCLineChartViewComponent alloc]init];
            [component setTitle:graphInfo[@"title"]];
            [component setPoints:graphInfo[@"data"]];
            [component setShouldLabelValues:NO];
            [component setColour:PCColorBlue];
            [components addObject:component];
        }
        [lineGraph setComponents:components];
        [lineGraph setXLabels:[results valueForKey:@"xLabels"]];
        
        
        
        UILabel *yourLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, 300, 20)];
        yourLabel.text = @"(BPM)";
        [yourLabel setTextColor:[UIColor blackColor]];
        [yourLabel setBackgroundColor:[UIColor clearColor]];
        [yourLabel setFont:[UIFont boldSystemFontOfSize:12]];
        [self.dataView addSubview:yourLabel];
        UILabel *yourLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(345, 400, 300, 20)];
        yourLabel2.text = @"(seconds)";
        [yourLabel2 setTextColor:[UIColor blackColor]];
        [yourLabel2 setBackgroundColor:[UIColor clearColor]];
        [yourLabel2 setFont:[UIFont boldSystemFontOfSize:12]];
        [self.dataView addSubview:yourLabel2];
        
        
        
        //////
        [self.graphStorage removeObjectAtIndex:0];
        
    }
    
    //HB graphing
    if([self.graphStorage count] < 10)
    {
        if(HBIndivStorageCounter < 30)
        {
            HBIndivStorage[HBIndivStorageCounter] = heartrate;
            HBIndivStorageCounter++;
        }
        else
        {
            for(int i = 0; i<30; i++)
            {
                HBAvrge += HBIndivStorage[i];
            }
            HBAvrge /= 30;
            
            [self.graphStorage addObject:[NSNumber numberWithInt:HBAvrge]];     //add Average to graphstorage MutableArray
            
            HBAvrge = 0;
            HBIndivStorageCounter = 0;
        }
    }
    else
    {
        NSArray *viewsToRemove = [self.dataView subviews];
        for (UIView *v in viewsToRemove)
        {
            [v removeFromSuperview];
        }
        
        PCLineChartView *lineGraph = [[PCLineChartView alloc]initWithFrame:CGRectMake(10,10, 700, 400)];
        [lineGraph setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        lineGraph.minValue = 0;
        lineGraph.maxValue = 200;
        [self.dataView addSubview:lineGraph];
        
        NSMutableArray *components = [NSMutableArray array];
        NSDictionary *results = [self getGraphData:self.graphStorage];
        NSMutableArray *data = [results valueForKey:@"data"];       //get the data from the self.graphstorage
        
        for(NSDictionary *graphInfo in data)
        {
            PCLineChartViewComponent *component = [[PCLineChartViewComponent alloc]init];
            [component setTitle:graphInfo[@"title"]];
            [component setPoints:graphInfo[@"data"]];
            [component setShouldLabelValues:NO];
            [component setColour:PCColorBlue];
            [components addObject:component];
        }
        [lineGraph setComponents:components];
        [lineGraph setXLabels:[results valueForKey:@"xLabels"]];
        [self.graphStorage removeObjectAtIndex:0];
        
        UILabel *yourLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, 300, 20)];
        yourLabel.text = @"(BPM)";
        [yourLabel setTextColor:[UIColor blackColor]];
        [yourLabel setBackgroundColor:[UIColor clearColor]];
        [yourLabel setFont:[UIFont boldSystemFontOfSize:12]];
        [self.dataView addSubview:yourLabel];
        UILabel *yourLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(345, 400, 300, 20)];
        yourLabel2.text = @"(seconds)";
        [yourLabel2 setTextColor:[UIColor blackColor]];
        [yourLabel2 setBackgroundColor:[UIColor clearColor]];
        [yourLabel2 setFont:[UIFont boldSystemFontOfSize:12]];
        [self.dataView addSubview:yourLabel2];
    }

    
    /////Calculates hand freq using FFT!!
    if(FFTcounter < 256)
    {
        accelStore[FFTcounter] = az;
        FFTcounter++;
    }
    else
    {
        
        const int log2n = 8;
        const int n = 1 << log2n;
        const int nOver2 = n / 2;
        
        FFTSetupD fftSetup = vDSP_create_fftsetupD (log2n, kFFTRadix2);
        
        double* input;
        
        DSPDoubleSplitComplex fft_data;
        
        int i;
        
        input = malloc(n * sizeof(double));
        fft_data.realp = malloc(nOver2 * sizeof(double));
        fft_data.imagp = malloc(nOver2 * sizeof(double));
        
        for(int j = 0; j<n; j++)
        {
            input[j] = (double)accelStore[j];
        }
        
        //printf("Input\n");
        
        for (i = 0; i < n; ++i)
        {
            //printf("%d: %8g\n", i, input[i]);
        }
        
        vDSP_ctozD((DSPDoubleComplex *)input, 2, &fft_data, 1, nOver2);
        
        //printf("FFT Input\n");
        
        for (i = 0; i < nOver2; ++i)
        {
            //printf("%d: %8g%8g\n", i, fft_data.realp[i], fft_data.imagp[i]);
        }
        
        vDSP_fft_zripD (fftSetup, &fft_data, 1, log2n, kFFTDirection_Forward);
        
        //printf("FFT output\n");
        
        for (i = 0; i < nOver2; ++i)
        {
            //printf("%d: %8g%8g\n", i, fft_data.realp[i], fft_data.imagp[i]);
        }
        
        for (i = 0; i < nOver2; ++i)
        {
            fft_data.realp[i] *= 0.5;
            fft_data.imagp[i] *= 0.5;
        }
        
        //printf("Scaled FFT output\n");
        
        double largest = 0;
        int largestIndex = 0;
        for (i = 1; i < nOver2; i++)
        {
            //printf("%d: %8g%8g\n", i, fft_data.realp[i], fft_data.imagp[i]);
            if(largest  <  (fft_data.realp[i]*fft_data.realp[i] + fft_data.imagp[i]*fft_data.imagp[i]))
            {
                largest = ((fft_data.realp[i]*fft_data.realp[i] + fft_data.imagp[i]*fft_data.imagp[i]));
                largestIndex = i;
            }
        }
        if(largestIndex > 30)
        {
            largestIndex = 0;
        }
        float freq = largestIndex*sampleFreq/256;
        self.handFreqLabel.text = [NSString stringWithFormat:@"%.1f Hz",freq];
        
        //////////////////////////////////////
        //////////////////////////////////////
        //Check's if seizure happens!!!!!!!!//
        //////////////////////////////////////
        //////////////////////////////////////
        if(sixtotwelvesec == false)
        {
            if(heartrate - self.myAvrgeHB >= 10)
            {
                sixtotwelvesec = true;
                sixtotwelvetime = (int)time_float;
            }
        }
        else
        {
            if((int)time_float - sixtotwelvetime > 6)
            {
                if(heartrate - self.myAvrgeHB > 10)
                {
                    /*UIAlertView  *alert =[[UIAlertView alloc] initWithTitle:@"SEIZURE SIGN #1"
                                                                    message: @"Heart rate pattern discovered!"
                                                                   delegate:self
                                                          cancelButtonTitle:@"Close"
                                                          otherButtonTitles: nil];
                    [alert show];     *///disable the alert
                    self.heartImage.hidden = NO;
                }
                else {
                    self.heartImage.hidden = YES;
                }
            }
        }
        if(freq-2.8 <= 0.5 && freq-2.8 >= -0.5 && self.seizureStartTime == 0)
        {
            self.seizureStartTime = (int)time_float;
        }
        
        if((int)time_float-self.seizureStartTime > 20 && (int)time_float-self.seizureStartTime < 30)
        {
            if(freq-1.2 <= 0.5 && freq-1.2 >= -0.5)
            {
                self.handSeizureOccurs = YES;
            }
        }
        if(self.handSeizureOccurs)
        {
            /*UIAlertView  *alert =[[UIAlertView alloc] initWithTitle:@"SEIZURE SIGN #2"
                                                            message: @" Seizure Hand Pattern Discovered!"
                                                           delegate:self
                                                  cancelButtonTitle:@"Close"
                                                  otherButtonTitles: nil];
            [alert show];*/ //disable the alert
            self.vibrationImage.hidden = NO;
            self.handSeizureOccurs = NO;
        }
        else {
            self.vibrationImage.hidden = YES;
        }

        FFTcounter = 0;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.axLabel.text = [NSString stringWithFormat:@"%d",ax];
        self.ayLabel.text = [NSString stringWithFormat:@"%d",ay];
        self.azLabel.text = [NSString stringWithFormat:@"%d",az];
        self.mxLabel.text = [NSString stringWithFormat:@"%d",mx];
        self.myLabel.text = [NSString stringWithFormat:@"%d",my];
        self.mzLabel.text = [NSString stringWithFormat:@"%d",mz];
        self.gxLabel.text = [NSString stringWithFormat:@"%d",gx];
        self.gyLabel.text = [NSString stringWithFormat:@"%d",gy];
        self.gzLabel.text = [NSString stringWithFormat:@"%d",gz];
        self.qwLabel.text = [NSString stringWithFormat:@"%.2f",q0];
        self.qxLabel.text = [NSString stringWithFormat:@"%.2f",q1];
        self.qyLabel.text = [NSString stringWithFormat:@"%.2f",q2];
        self.qzLabel.text = [NSString stringWithFormat:@"%.2f",q3];
        self.yawLabel.text = [NSString stringWithFormat:@"%.2f",pitch];
        self.pitchLabel.text = [NSString stringWithFormat:@"%.2f",yaw];
        self.rollLabel.text = [NSString stringWithFormat:@"%.2f",roll];
        self.heartbeatLabel.text = [NSString stringWithFormat:@"%d",heartrate];
        self.timestampLabel.text = [NSString stringWithFormat:@"%d msec", timestamp];
    });
    
    if(self.isRecording)        //For the video recording
    {
        if(self.startingTimeStampBOOL)
        {
            self.startingTimeStamp = (int)time_float;
            
            //Only declare these things once until the next recording
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [NSTimeZone localTimeZone];
            [dateFormatter setDateFormat:@"hh:mm:ss a"];
            
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *file = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"data_%@.csv", self.dateString]];
                                                                                 //NSLog(@"Sensor log file: %@", file);
                                                                                 
                                                                                 self.sensorLogFileHandle = [NSFileHandle fileHandleForWritingAtPath:file];
                                                                                 if (self.sensorLogFileHandle == nil) {
                                                                                     [[NSFileManager defaultManager] createFileAtPath:file contents:nil attributes:nil];
                                                                                     self.sensorLogFileHandle = [NSFileHandle fileHandleForWritingAtPath:file];
                                                                                 }
            NSString *headerString = [NSString stringWithFormat:@"ax,ay,az,mx,my,mz,gx,gy,gz,HR,timestamp\n"];
            [self.sensorLogFileHandle writeData:[headerString dataUsingEncoding:NSUTF8StringEncoding]];
            
            self.startingTimeStampBOOL = NO;
        }
        int seconds = ((int)time_float-self.startingTimeStamp) % 60;
        int minutes = (((int)time_float-self.startingTimeStamp) / 60) % 60;
        int hours = ((int)time_float-self.startingTimeStamp) / 3600;

        self.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
        self.timeLabel.textColor = [UIColor redColor];
        
        
        //OLD METHOD OF WRITING TO FILE
//        [self.dataDict addObject:[NSString stringWithFormat:@"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",ax,ay,az,mx,my,mz,gx,gy,gz,heartrate,timestamp]];            //The format of the data in XML format
//        dictSizeCounter++;
        
        
        NSString *stringToWrite = [NSString stringWithFormat:@"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",ax,ay,az,mx,my,mz,gx,gy,gz,heartrate,timestamp];
        [self.sensorLogFileHandle writeData:[stringToWrite dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
        self.timeLabel.text = @"";
        self.startingTimeStampBOOL = YES;
        

    }
    /*
    ///SAVING AS A JSON FILE IF RECORDING
    if(self.isRecording)
    {
        if(dictSizeCounter < maxSizeOfDict)
        {
            //NSMutable Array
            [self.dataDict addObject:[NSString stringWithFormat:@"ax%d, ay%d, az%d, mx%d, my%d, mz%d, gx%d, gy%d, gz%d, hb%d, time%f",ax,ay,az,mx,my,mz,gx,gy,gz,heartrate,time_float]];
            dictSizeCounter++;
        }
        else        //Once dictionary size counter hits the maximum as defined, save it into a file
        {
            dictSizeCounter = 0;
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
            
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"data.xml"];
            [self.dataDict writeToFile:filePath atomically:YES];
            
            [self uploadPatientDataByURL:filePath];
            [self.dataDict removeAllObjects];
            NSLog(@"MAX FILE SIZE REACHED FOR DATA");
        }
    }
     */
    
    
}


# pragma mark - UPLOADING functions

/*
-(void) pushUpload
{
    NSData *movie = [NSData dataWithContentsOfFile:@"red-dot-hi.png"];
    NSString *urlString = @"http://137.132.165.89/PD/upload.php";
    
    NSMutableURLRequest *request  = [[NSMutableURLRequest alloc]init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@%@%@", @"Content-Disposition: form-data; name=\"userfile\"; filename=\"",_mainURL,@"\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:movie]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    //NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
}
 
 */

-(void) uploadDataAndMovie{

    if (doOnce){
        
        
        self.queueFTP = [[QueueFTP alloc] init:@"http://137.132.165.89/uploadz.php"];   //put inside the folder "uploads"
        [self.queueFTP setDelegate:self];
        
    }
    /********************************************************************************
     * Upload files in documents directory
     ********************************************************************************/
    
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [searchPaths objectAtIndex:0];
    NSString *folderPath =documentPath;
    NSFileManager* fileManager = [[NSFileManager alloc] init];
    NSDirectoryEnumerator* en = [fileManager enumeratorAtPath:folderPath];
    NSLog(@"en is  %@  path is %@", en, folderPath);
    NSString* file;
    //NSLog(@"hi im starting to upload very soon");       //2nd phase of uploading
    
    if(!self.fileUploadManager)
        self.fileUploadManager = [[FileUploadManager alloc] init];
    
    while (file = [en nextObject]) {        //First recording will upload everything in the Documents folder, 2nd time onwards only the latest files, because it keeps track of the pointer (en)
        if ((
             ([file rangeOfString:@".csv"
                          options:NSCaseInsensitiveSearch].location != NSNotFound) ||
             ([file rangeOfString:@".jpg"
                          options:NSCaseInsensitiveSearch].location != NSNotFound)) &&
            ![self.fileUploadManager isFileUploading:file]){
            
            
            NSString *path = [folderPath stringByAppendingFormat:
                              @"/%@",file];
            [self.queueFTP addFile:path];
            [self.fileUploadManager addPendingFileName:file];
            [self.queueFTP beginExecution];
            /*if (doOnce){
             [self.queueFTP beginExecution];
             }*/
            
            //[fileManager removeItemAtPath:path error:nil];        //don't remove for now
            
            //NSLog(@"Removed Item at %@", path);
            
            //NSLog(@"QueueFTP: %@",path);
            //self.queueFTP.networkQueue.bytesUploadedSoFar
        }
        else if(self.uploadVideoSwitchOn)        //Toggled to the right, need to upload video.
        {
            if(([file rangeOfString:@".mov"
                            options:NSCaseInsensitiveSearch].location != NSNotFound) &&
                ![self.fileUploadManager isFileUploading:file]){
                    NSString *path = [folderPath stringByAppendingFormat:
                                      @"/%@",file];         // in this case is /.mov
                
                NSRange range = [path rangeOfString:@"MOVx"];
                if (range.location != NSNotFound)        //If found the 'x' word, do nothing
                {
                    NSLog(@"MOVx skipped");
                } else {
                    [self.queueFTP addFile:path];
                    [self.fileUploadManager addPendingFileName:file];
                    [self.queueFTP beginExecution];
                    
                    
                    //[fileManager removeItemAtPath:path error:nil];      //don't remove for now
                    //[self.fileUploadManager removeUploadedFile:path];

                    //NSLog(@"Removed Item at %@", path);
                }
                
            }
        }
    }
    doOnce = NO;
}
-(void) fileUploadDone:(BOOL)success forFile:(NSString*)fileName{
    
    NSFileManager* fm = [[NSFileManager alloc] init];
    NSError* err = nil;
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [searchPaths objectAtIndex:0];
    /*
     NSLog(@"file name before %@, length %lu",fileName, (unsigned long)[fileName length]);
     if ([fileName length] > 27){
     
     int len = [fileName length] - 26;
     [fileName substringWithRange:NSMakeRange(13, len)];
     }
     NSLog(@"file name after %@",fileName);*/
    
    
    if (success == NO) {
        
        NSLog(@"upload failed");
    }
    else{
        //NSArray *directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentPath error:&err];
        if ([fileName length] != 0 && [fileName rangeOfString:@".xml"
                                                    options:NSCaseInsensitiveSearch].location == NSNotFound){
            
            NSLog(@"upload success, removing %@ from:", fileName);
            
            BOOL res = [fm removeItemAtPath:[documentPath stringByAppendingPathComponent:fileName] error:&err];
            
            if (res == NO) {
                NSLog(@"error removing file: %@, %@", fileName,err);
                
            }else{
                NSLog(@"file upload done, removed %@",fileName);
            }
            
        }
        else {
            
            NSLog(@"No file removed, Filename passed as nil or empty:  %@",fileName);
            
        }
        
        
    }
}
-(void) allFilesUploaded{
    NSLog(@"all file upload(s) done for bar graphs");
    doOnce = YES;
}


-(void)uploadPatientDataByURL:(NSString *)filePath {
    
    NSLog(@"uploading CSV file: %@",[[filePath pathComponents] lastObject]);    //only display the name of the file, not the directory
    
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    NSMutableURLRequest *request =
    [serializer multipartFormRequestWithMethod:@"POST"
                                     URLString:@"http://137.132.165.89/uploadz.php"
                                    parameters:nil
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         [formData appendPartWithFileURL:[NSURL fileURLWithPath:filePath]
                                                    name:@"userfile"
                                                   error:nil];
                     }
                                         error:nil];
    
    AFHTTPRequestOperation *operation =
    [self.manager HTTPRequestOperationWithRequest:request
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              
                                              NSDictionary *jsonResponse;
                                              if ([responseObject isKindOfClass:[NSData class]]) {
                                                  jsonResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                                              }
                                              else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                                  jsonResponse = responseObject;
                                              } else {
                                                  NSLog(@"Response unrecognized: %@", jsonResponse);
                                                  return;
                                              }
                                              NSLog(@"Upload CSV response: %@", jsonResponse);
                                              
                                              //delete file if successful
                                              if ([[jsonResponse objectForKey:@"phan"] isKindOfClass:[NSString class]] && [[jsonResponse objectForKey:@"phan"] isEqualToString:@"shi wen"]) {
                                                  
                                                  NSLog(@"File %@ uploaded successfully.",[jsonResponse objectForKey:@"filename"]);
                                                  
                                                  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                                  NSString *documentsDirectory = [paths objectAtIndex:0];
                                                  NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[jsonResponse objectForKey:@"filename"]]];
                                                  
                                                  NSFileManager *fileManager = [NSFileManager defaultManager];
                                                  [fileManager removeItemAtPath:filePath error:nil];
                                                  
                                              } else {
                                                  //do nothing
                                              }
                                              
                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              //do nothing
                                          }];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        //do nothing
        
    }];         //Show progress of upload
    
    
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{ //allow to upload for some time after app closes
        //do nothing
        //in future can send a notification to user that there are unuploaded videos
    }];
    [self.manager.operationQueue addOperation:operation];
}

- (IBAction)uploadVideoSwitchValueChanged:(UISwitch *)sender {
    /*if ([sender isOn]) {
     [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"shouldAutoplay"];
     } else {
     [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"shouldAutoplay"];
     }*/
    if(sender.isOn) self.uploadVideoSwitchOn = YES;
    else self.uploadVideoSwitchOn = NO;
}

#pragma mark - Toggle Camera functions

-(void)toggleCamera:(BOOL)front
{
    NSArray *devices = [AVCaptureDevice devices];       //capture all the devices in NSArray
    AVCaptureDevice *frontCamera;
    AVCaptureDevice *backCamera;
    
    for (AVCaptureDevice *device in devices) {
        
        NSLog(@"Device name: %@", [device localizedName]);
        
        if ([device hasMediaType:AVMediaTypeVideo]) {
            
            if ([device position] == AVCaptureDevicePositionBack) {
                NSLog(@"Device position : back");
                backCamera = device;
            }
            else {
                NSLog(@"Device position : front");
                frontCamera = device;
            }
        }
    }
    [[self avSession] beginConfiguration];  //Must always start with beginConfiguration and end with commitConfiguration
    
    NSError *error = nil;
    frontFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];       //The device input to add later
    AVCaptureInput *bCamera;
    AVCaptureInput *fCamera;
    backFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];         //The device input to add later
    
    if (front)      //if want to change to front camera
    {
        if(self.backToFrontFirstEntry)
        {
            bCamera = [self.avSession.inputs objectAtIndex:0];      //bcoz gonna remove back camera;
            backFacingCameraInput = bCamera;
            self.backToFrontFirstEntry = FALSE;    //never enter this loop again
        }
        else {
            bCamera = [self.avSession.inputs objectAtIndex:1];      //bcoz gonna remove back camera;
            backFacingCameraInput = bCamera;
        }
        [[self avSession] removeInput:backFacingCameraInput];
        
        frontFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if ([[self avSession] canAddInput:frontFacingCameraDeviceInput]) {
            [[self avSession] addInput:frontFacingCameraDeviceInput];
        } else {
            NSLog(@"Couldn't add front facing video input");
        }
    }
    else        //else, change to back camera
    {
        
        fCamera = [self.avSession.inputs objectAtIndex:1];      //bcoz need to remove front camera later on.
        frontFacingCameraInput = fCamera;
        
        [self.avSession removeInput:frontFacingCameraInput];
        
        if ([[self avSession] canAddInput:backFacingCameraDeviceInput]) {
            [[self avSession] addInput:backFacingCameraDeviceInput];
        } else {
            NSLog(@"Couldn't add back facing video input");
        }
    }
    
    [[self avSession] commitConfiguration];     //Must always start with beginConfiguration and end with commitConfiguration
}


- (IBAction)switchCameraTapped:(id)sender {
    
    [self.avSession stopRunning];
    if (self.isFrontCameraSelected) {
        self.isFrontCameraSelected = NO;
    } else {
        self.isFrontCameraSelected = YES;
        
    }
    
    [self toggleCamera:self.isFrontCameraSelected];
    [self.avSession startRunning];
}




# pragma mark - Quarternion methods


volatile float q0 = 1.0f;
volatile float q1 = 0.0f;
volatile float q2 = 0.0f;
volatile float q3 = 0.0f;
static volatile float beta = betaDef;

/////////////////////
/////////////////////
/////////////////////
/////////////////////

void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz)
{
    float recipNorm;
    float s0, s1, s2, s3;
    float qDot1, qDot2, qDot3, qDot4;
    float hx, hy;
    float _2q0mx, _2q0my, _2q0mz, _2q1mx, _2bx, _2bz, _4bx, _4bz, _2q0, _2q1, _2q2, _2q3, _2q0q2, _2q2q3, q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;
    
    // Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
    if((mx == 0.0f) && (my == 0.0f) && (mz == 0.0f))
    {
        MadgwickAHRSupdateIMU(gx, gy, gz, ax, ay, az);
        return;
    }
    
    // Rate of change of quaternion from gyroscope
    qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
    qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
    qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
    qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);
    
    // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
    if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f)))
    {
        
        // Normalise accelerometer measurement
        recipNorm = invSqrt(ax * ax + ay * ay + az * az);
        ax *= recipNorm;
        ay *= recipNorm;
        az *= recipNorm;
        
        // Normalise magnetometer measurement
        recipNorm = invSqrt(mx * mx + my * my + mz * mz);
        mx *= recipNorm;
        my *= recipNorm;
        mz *= recipNorm;
        
        // Auxiliary variables to avoid repeated arithmetic
        _2q0mx = 2.0f * q0 * mx;
        _2q0my = 2.0f * q0 * my;
        _2q0mz = 2.0f * q0 * mz;
        _2q1mx = 2.0f * q1 * mx;
        _2q0 = 2.0f * q0;
        _2q1 = 2.0f * q1;
        _2q2 = 2.0f * q2;
        _2q3 = 2.0f * q3;
        _2q0q2 = 2.0f * q0 * q2;
        _2q2q3 = 2.0f * q2 * q3;
        q0q0 = q0 * q0;
        q0q1 = q0 * q1;
        q0q2 = q0 * q2;
        q0q3 = q0 * q3;
        q1q1 = q1 * q1;
        q1q2 = q1 * q2;
        q1q3 = q1 * q3;
        q2q2 = q2 * q2;
        q2q3 = q2 * q3;
        q3q3 = q3 * q3;
        
        // Reference direction of Earth's magnetic field
        hx = mx * q0q0 - _2q0my * q3 + _2q0mz * q2 + mx * q1q1 + _2q1 * my * q2 + _2q1 * mz * q3 - mx * q2q2 - mx * q3q3;
        hy = _2q0mx * q3 + my * q0q0 - _2q0mz * q1 + _2q1mx * q2 - my * q1q1 + my * q2q2 + _2q2 * mz * q3 - my * q3q3;
        _2bx = sqrt(hx * hx + hy * hy);
        _2bz = -_2q0mx * q2 + _2q0my * q1 + mz * q0q0 + _2q1mx * q3 - mz * q1q1 + _2q2 * my * q3 - mz * q2q2 + mz * q3q3;
        _4bx = 2.0f * _2bx;
        _4bz = 2.0f * _2bz;
        
        // Gradient decent algorithm corrective step
        s0 = -_2q2 * (2.0f * q1q3 - _2q0q2 - ax) + _2q1 * (2.0f * q0q1 + _2q2q3 - ay) - _2bz * q2 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q3 + _2bz * q1) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q2 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
        s1 = _2q3 * (2.0f * q1q3 - _2q0q2 - ax) + _2q0 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q1 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + _2bz * q3 * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q2 + _2bz * q0) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q3 - _4bz * q1) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
        s2 = -_2q0 * (2.0f * q1q3 - _2q0q2 - ax) + _2q3 * (2.0f * q0q1 + _2q2q3 - ay) - 4.0f * q2 * (1 - 2.0f * q1q1 - 2.0f * q2q2 - az) + (-_4bx * q2 - _2bz * q0) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * q1 + _2bz * q3) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * q0 - _4bz * q2) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
        s3 = _2q1 * (2.0f * q1q3 - _2q0q2 - ax) + _2q2 * (2.0f * q0q1 + _2q2q3 - ay) + (-_4bx * q3 + _2bz * q1) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * q0 + _2bz * q2) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * q1 * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
        recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
        s0 *= recipNorm;
        s1 *= recipNorm;
        s2 *= recipNorm;
        s3 *= recipNorm;
        
        // Apply feedback step
        qDot1 -= beta * s0;
        qDot2 -= beta * s1;
        qDot3 -= beta * s2;
        qDot4 -= beta * s3;
    }
    
    // Integrate rate of change of quaternion to yield quaternion
    q0 += qDot1 * (1.0f / sampleFreq);
    q1 += qDot2 * (1.0f / sampleFreq);
    q2 += qDot3 * (1.0f / sampleFreq);
    q3 += qDot4 * (1.0f / sampleFreq);
    
    
    // Normalise quaternion
    recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
    q0 *= recipNorm;
    q1 *= recipNorm;
    q2 *= recipNorm;
    q3 *= recipNorm;
}

//---------------------------------------------------------------------------------------------------
// IMU algorithm update

void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az)
{
    float recipNorm;
    float s0, s1, s2, s3;
    float qDot1, qDot2, qDot3, qDot4;
    float _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2 ,_8q1, _8q2, q0q0, q1q1, q2q2, q3q3;
    
    // Rate of change of quaternion from gyroscope
    qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
    qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
    qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
    qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);
    
    // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
    if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f)))
    {
        
        // Normalise accelerometer measurement
        recipNorm = invSqrt(ax * ax + ay * ay + az * az);
        ax *= recipNorm;
        ay *= recipNorm;
        az *= recipNorm;
        
        // Auxiliary variables to avoid repeated arithmetic
        _2q0 = 2.0f * q0;
        _2q1 = 2.0f * q1;
        _2q2 = 2.0f * q2;
        _2q3 = 2.0f * q3;
        _4q0 = 4.0f * q0;
        _4q1 = 4.0f * q1;
        _4q2 = 4.0f * q2;
        _8q1 = 8.0f * q1;
        _8q2 = 8.0f * q2;
        q0q0 = q0 * q0;
        q1q1 = q1 * q1;
        q2q2 = q2 * q2;
        q3q3 = q3 * q3;
        
        // Gradient decent algorithm corrective step
        s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
        s1 = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
        s2 = 4.0f * q0q0 * q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
        s3 = 4.0f * q1q1 * q3 - _2q1 * ax + 4.0f * q2q2 * q3 - _2q2 * ay;
        recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
        s0 *= recipNorm;
        s1 *= recipNorm;
        s2 *= recipNorm;
        s3 *= recipNorm;
        
        // Apply feedback step
        qDot1 -= beta * s0;
        qDot2 -= beta * s1;
        qDot3 -= beta * s2;
        qDot4 -= beta * s3;
    }
    
    // Integrate rate of change of quaternion to yield quaternion
    q0 += qDot1 * (1.0f / sampleFreq);
    q1 += qDot2 * (1.0f / sampleFreq);
    q2 += qDot3 * (1.0f / sampleFreq);
    q3 += qDot4 * (1.0f / sampleFreq);
    
    // Normalise quaternion
    recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
    q0 *= recipNorm;
    q1 *= recipNorm;
    q2 *= recipNorm;
    q3 *= recipNorm;
}
float invSqrt(float x)
{
    float halfx = 0.5f * x;
    float y = x;
    long i = *(long*)&y;
    i = 0x5f3759df - (i>>1);
    y = *(float*)&i;
    y = y * (1.5f - (halfx * y * y));
    return y;
}

@end

