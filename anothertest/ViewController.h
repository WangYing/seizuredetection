//
//  ViewController.h
//  blue
//
//  Created by MacMini2 on 5/20/15.
//  Copyright (c) 2015 NUS. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <MediaPlayer/MediaPlayer.h>
#include <Accelerate/Accelerate.h>
@import GLKit;
@import OpenGLES;

@interface ViewController : UIViewController <CBCentralManagerDelegate,CBPeripheralDelegate>

@property CBCentralManager *centralManager;
@property CBPeripheral *discoveredPeripheral;

#define SERVICE_UUID    @"FFF0";
#define CHARACTERISTIC_UUID    @"FFF1";
@property (strong, nonatomic) MPMoviePlayerController *movie;
@end

