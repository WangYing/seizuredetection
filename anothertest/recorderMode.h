//
//  recorderMode.h
//  anotherTest
//
//  Created by nus on 6/24/15.
//  Copyright (c) 2015 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AVFoundation;
@import CoreMedia;
@import MediaPlayer;
@import AssetsLibrary;
#import <CoreBluetooth/CoreBluetooth.h>
#include <Accelerate/Accelerate.h>
@import GLKit;
@import OpenGLES;       //important for cubes
@import QuartzCore;     //important for cubes
@import MobileCoreServices;
@import SystemConfiguration;
#import "QueueFTP.h"

@interface recorderMode : UIViewController <AVCaptureFileOutputRecordingDelegate,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, CBCentralManagerDelegate,CBPeripheralDelegate,UITableViewDelegate,QueueFTPDelegate>

@property (weak, nonatomic) IBOutlet UIView *cameraView;

@property CBCentralManager *secondOldCentralManager;
@property CBPeripheral *secondOldDiscoveredPeripheral;
@property int myAvrgeHB;

#define SERVICE_UUID    @"FFF0";
#define CHARACTERISTIC_UUID    @"FFF1";

-(void)toggleCamera:(BOOL)front;

@property BOOL isRecording;
@property (strong, nonatomic) IBOutlet UIButton *recordButton;
- (IBAction)recordPress:(UIButton *)sender;


@end
